﻿(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies'])
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];
    function config($routeProvider, $locationProvider, $httpProvider) {
        //routing in angular js
        $routeProvider
            .when('/', {
                controller: 'mainController',
                templateUrl: 'views/main.html',
                controllerAs: 'ctrl'
            })
            .otherwise({ redirectTo: '/' })
    }
})();