(function () {
    'use strict';

    angular
        .module('app')
        .factory('gistData', gistData);

    gistData.$inject = ['$http'];
    function gistData($http) {
        let service = {};
        service.loadUserData = loadUserData;
        service.loadFolkData = loadFolkData;
        return service;       

        //load gist data according to username
        function loadUserData(username){
            const url = "https://api.github.com/users/" + username + "/gists";
            return $http.get(url).then(handleSuccess, handleError('Error getting User Gist'));
        }

        //load folk data according to gist
        function loadFolkData(gistId){
            const url = "https://api.github.com/gists/" + gistId + "/forks";          
            return $http.get(url).then(handleSuccess, handleError('Error getting Gist Folks'));
        }

        // private functions
        function handleSuccess(res) {
            const response = {
                data: res.data
            }
            return response;
        }

        function handleError(error) {
            return function () {
                return { success: false, error : error};
            };
        }
    }

})();
