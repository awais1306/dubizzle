﻿(function () {
    'use strict';

    var app = angular
        .module('app')
        .controller('mainController', mainController);

    mainController.$inject = ['gistData', '$rootScope' , '$filter', 'FlashService'];
    function mainController(gistData, $rootScope , $filter , FlashService) {
        var ctrl = this;  
        $rootScope.getUserForks = () => {
            //start game
            const username = ctrl.username;           
            ctrl.dataLoading = true;

            if(!username || username == ""){
                FlashService.Error('Please Enter Username');
                ctrl.dataLoading = false;
                return;
            }
            getUserData(username).then((data) => {  
                //check if there is any error.                            
                if (data.hasOwnProperty("error")) {
                    if (data.errorAt == "user data") {
                        FlashService.Error('Error Fetching Gist Data');
                        ctrl.dataLoading = false;
                        $rootScope.$apply();  
                        return false;
                    }
                    else {
                        FlashService.Error("There was an error from server. \nPlease try again later!");
                        ctrl.dataLoading = false;
                        $rootScope.$apply();  
                        return false;
                    }
                    ctrl.userGists = data;
                    ctrl.dataLoading = false;
                    $rootScope.$apply();  
                }else{
                    //return the data to view
                    ctrl.userGists = data;
                    ctrl.dataLoading = false;
                    $rootScope.$apply();  
                }                      
            }); 
        }
        
        //get user data from gist api
        const getUserData = async(username) => {  
            //api call from model     
            let response = await(gistData.loadUserData(username));
            if (response.hasOwnProperty("error")) {
                response.errorAt = "user data";
                return response;
            }else{
                let data = response.data;
                //new array gist + folks index
                let folksGist = [];
                for(let gist of data){
                    //getting folks data
                    let gistData = await mapFolksToGist(gist);
                    //push in  array folks index
                    folksGist[folksGist.length] = gistData;
                }
                return folksGist;
            }
        };

        //map folks to respective gist by calling folk api.
        const mapFolksToGist = async(gist) =>{
            let response = await(gistData.loadFolkData(gist.id));
            if (response.hasOwnProperty("error")) {
                response.data = [];
                return response;
            }else{
                //if no error return folks data
                gist.folks = response.data;
                return gist;
            }
            
        }
    }
})();